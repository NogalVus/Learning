@echo off
title Synchronizing...
D:
cd nono/documents/coding/workspace/learning
cd
git add src/ debug/ .settings/ .project .cproject GitSync.bat
git status
set /p confirm=Are you sure you want to sync this change?(Y/n)
if "%confirm%"=="Y" (
	git commit
	git status
	set /p confirm2=Are you sure that you want to sync this change to GitHub?(Y/n)
	if "%confirm2%"=="Y" (
		git push -u origin master
		git status
		pause
	)
)
