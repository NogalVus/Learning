/*
 * ClassTest.cpp
 *
 *  Created on: Feb 17, 2018
 *      Author: HJ
 */
#include <iostream>
using namespace std;

class People{
	public:
		char name[50];
		People (int ageinit = 0,char sexinit = 'M'){
			age = ageinit;
			sex = sexinit;
			cout<<"And now please name your new member"<<endl;
		}
	private:
		int age;
		char sex;
};
